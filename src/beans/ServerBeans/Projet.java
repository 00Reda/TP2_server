/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.ServerBeans;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Vector;

/**
 *
 * @author IdeaPad
 */
public class Projet implements Serializable{
        private static final long serialVersionUID = 1L;
        private int id_projet;
	private String nom_projet;
	private String date_projet;
	private String owner_projet;
        private String background_projet ;
        public Projet(){
            
        }
        
        public Projet(int id_projet){
            this.id_projet=id_projet;
        }

        public Projet(String nom_projet, String owner_projet, String background_projet) {
            this.nom_projet = nom_projet;
            this.owner_projet = owner_projet;
            this.background_projet = background_projet;
        }
        
        public Projet(int id_projet ,String nom_projet, String date_projet, String owner_projet) {
            this.id_projet=id_projet;
            this.nom_projet = nom_projet;
            this.date_projet = date_projet;
            this.owner_projet = owner_projet;
        }

        public Projet(int id_projet, String nom_projet, String date_projet, String owner_projet, String background_projet) {
            this.id_projet = id_projet;
            this.nom_projet = nom_projet;
            this.date_projet = date_projet;
            this.owner_projet = owner_projet;
            this.background_projet = background_projet;
        }

        public String getBackground_projet() {
            return background_projet;
        }

        public void setBackground_projet(String background_projet) {
            this.background_projet = background_projet;
        }
        
        public int getId_projet() {
            return id_projet;
        }

        public void setId_projet(int id_projet) {
            this.id_projet = id_projet;
        }

	public String getNom_projet(){
		return nom_projet;
	}

	public void setNom_projet(String nom_projet){
		this.nom_projet=nom_projet;
	}

	public String getDate_projet(){
		return date_projet;
	}

	public void setDate_projet(String date_projet){
		this.date_projet=date_projet;
	}

	public String getOwner_projet(){
		return owner_projet;
	}

	public void setOwner_projet(String owner_projet){
		this.owner_projet=owner_projet;
	}
        
        
         public static Vector getClassProprties(){
             
            Field[] fs= (new Projet()).getClass().getDeclaredFields();
            Vector<String> r=new Vector<String>();
            for (int i = 0; i < fs.length; i++) {
                StringBuffer sb=new StringBuffer(fs[i].toString());
                 String str=sb.substring(sb.lastIndexOf("."));
                    if(!str.contains("serial"))
                     r.add(str);
            }
                return r;
            }
         
        public Vector ObjectToVector(){
            
            Vector r=new Vector();
            r.add(this.id_projet+"");
            r.add(this.nom_projet);
            r.add(this.date_projet);
            r.add(this.owner_projet);
            r.add(this.background_projet);
            
            return r;
        }

    @Override
    public String toString() {
        return "Projet{" + "id_projet=" + id_projet + ", nom_projet=" + nom_projet + ", date_projet=" + date_projet + ", owner_projet=" + owner_projet + ", background_projet=" + background_projet + '}';
    }
        
        
        
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.ServerBeans;

import beans.ServerBeans.user;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Vector;

/**
 *
 * @author IdeaPad
 */
public class partage implements Serializable{
    private static final long serialVersionUID = 1L;
     private user users;
     private String date_partage;
     private Projet p;
     
    public partage() {
    }

    public partage(user users, String date_partage, Projet p) {
        this.users = users;
        this.date_partage = date_partage;
        this.p = p;
    }

    public partage(user users,Projet p) {
        this.users = users;
        this.p = p;
    }

    public Projet getP() {
        return p;
    }

    public void setP(Projet p) {
        this.p = p;
    }

    public user getUsers() {
        return users;
    }

    public void setUsers(user users) {
        this.users = users;
    }

    public String getDate_partage() {
        return date_partage;
    }

    public void setDate_partage(String date_partage) {
        this.date_partage = date_partage;
    }
     
    
     public static Vector getClassProprties(){
        
        Vector<String> r=new Vector<String>();
        r.add("username ");
        r.add("email ");
        r.add("role ");
        r.add("date ajout ");
        r.add("date partage  ");
        return r;
    }
    public Vector ObjectToVector(){
        Vector r=new Vector();
        r.add(users.getUsername());
        r.add(users.getEmail());
        r.add(users.getRole());
        r.add(users.getDate_ajout());
        r.add(this.date_partage);
        return r;
    }

     
    
}

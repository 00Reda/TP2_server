/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beans.ServerBeans;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Vector;
import tools.Security;

/**
 *
 * @author IdeaPad
 */
public class user implements Serializable{
    private static final long serialVersionUID = 1L;
    private String username;
    private String password ;
    private String email;
    private String role;
    private String date_ajout;

    
    public user() {
        
    }
    
    
    public user(String username, String password,String role) {
        this.username = username;
        this.password = Security.getCryptedPassword( password);
        this.role=role;
    }

    @Override
    public boolean equals(Object obj) {
       user u =(user) obj;
       if(this.username.equals(u.username)) return true;
       else return false;
       
    }

    public user(String username, String password, String email, String role, String date_ajout) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.role = role;
        this.date_ajout = date_ajout;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDate_ajout() {
        return date_ajout;
    }

    public void setDate_ajout(String date_ajout) {
        this.date_ajout = date_ajout;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    public static Vector getClassProprties(){
        Field[] fs= (new user()).getClass().getDeclaredFields();
        Vector<String> r=new Vector<String>();
        for (int i = 0; i < fs.length; i++) {
            StringBuffer sb=new StringBuffer(fs[i].toString());
            String str=sb.substring(sb.lastIndexOf("."));
            if(!str.contains("serial"))
             r.add(str);
        }
        return r;
    }
    public Vector ObjectToVector(){
        Vector r=new Vector();
        r.add(username);
        r.add(password);
        r.add(email);
        r.add(role);
        r.add(date_ajout);
        return r;
    }

    @Override
    public String toString() {
        return "user{" + "username=" + username + ", password=" + password + ", email=" + email + ", role=" + role + ", date_ajout=" + date_ajout + '}';
    }
    
    
}

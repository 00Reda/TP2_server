/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;
import model.projetDAO;
import view.projet.ListProjetFrame;
import beans.ServerBeans.Projet;
import beans.forme;
import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import javax.swing.JOptionPane;
import view.projet.VisualisationFrame;
import tp1.panels.ZoneDessin;

/**
 *
 * @author IdeaPad
 */
public class projetController {
    
    public void ListeProjetAction(String username){
        ListProjetFrame lf= new ListProjetFrame();
        DefaultTableModel tableModel=new DefaultTableModel(Projet.getClassProprties(), 0);
        lf.getProjectJT().setModel(tableModel);
        
        try {
            
            projetDAO pd=new projetDAO();
            ArrayList<Projet>listes =pd.Lister( username);
            for (int i = 0; i < listes.size(); i++) {
                tableModel.addRow(listes.get(i).ObjectToVector());
            }
           
           
        } catch (Exception e) {
            
              JOptionPane.showMessageDialog(null, e.getMessage(), "erreur", JOptionPane.ERROR_MESSAGE);

        }
        
        
        
        lf.setVisible(true);
    }
    
     public boolean DeleteProjetAction(Projet p){
        boolean r=true;
        try {
            projetDAO model=new projetDAO();
            r= model.Suppression(p);
        } catch (Exception ex) {
            r=false;
            JOptionPane.showMessageDialog(null, ex.getMessage(), "erreur", JOptionPane.ERROR_MESSAGE);
        }
        
        return r;
    }
     
     public boolean GetProjectVisualisationAction(Projet p){
         
         VisualisationFrame zd=new VisualisationFrame();
         
         try {
             projetDAO model=new projetDAO();
             ArrayList<forme> list =new ArrayList<>();
             list=model.GetProjectForms(p);
             //System.out.println(Color.decode(p.getBackground_projet()));
            zd.getZoneDessin().setListOfDrawedForms(list);
            zd.getZoneDessin().setBackground(Color.decode(p.getBackground_projet()));
            zd.setVisible(true);
            zd.setTitle("nom du projet : "+p.getNom_projet()+" et le proprietaire : "+p.getOwner_projet());
           // zd.paint(zd.getGraphics());
            //JOptionPane.showMessageDialog(null, "done ",":)", JOptionPane.INFORMATION_MESSAGE);
            return true;
        } catch (Exception ex ) {
           ex.printStackTrace();
           JOptionPane.showMessageDialog(null,"Erreur : "+ ex.getMessage(),":(", JOptionPane.ERROR_MESSAGE);
           
        }       
        return false;
         
        
     }
     
      
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import beans.ServerBeans.Projet;
import beans.ServerBeans.partage;
import beans.ServerBeans.user;
import java.util.ArrayList;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import model.partageDAO;
import view.partage.AddCollaborateurFrame;
import view.partage.ListePartageFrame;

/**
 *
 * @author IdeaPad
 */
public class PartageController {
    
    public void ListePartageAction(Projet p){
        ListePartageFrame lp= new ListePartageFrame(p);
        DefaultTableModel tableModel=new DefaultTableModel(partage.getClassProprties(), 0);
        lp.getPartageJT().setModel(tableModel);
        
        try {
            
            partageDAO pd=new partageDAO();
            ArrayList<partage>listes =pd.Lister(p);
            for (int i = 0; i < listes.size(); i++) {
                tableModel.addRow(listes.get(i).ObjectToVector());
            }
           
           
        } catch (Exception e) {
            
              JOptionPane.showMessageDialog(null, e.getMessage(), "erreur", JOptionPane.ERROR_MESSAGE);

        }
        
        
        
        lp.setVisible(true);
    }
    
    public boolean DeleteProjetAction(partage p){
        boolean r=true;
        try {
            partageDAO model=new partageDAO();
            r= model.Suppression(p);
        } catch (Exception ex) {
            r=false;
            JOptionPane.showMessageDialog(null, ex.getMessage(), "erreur", JOptionPane.ERROR_MESSAGE);
        }
        
        return r;
    }
    
    public void PreparerPartageAction(Projet p){
        
        AddCollaborateurFrame frame=new AddCollaborateurFrame(p);
         
        try {
            partageDAO model=new partageDAO();
            ArrayList <user> listes=model.getUserNoCollaborer(p);
            DefaultListModel m=new DefaultListModel();
             for (int i = 0; i < listes.size(); i++) {
                m.addElement(listes.get(i).getUsername());
            }
            frame.getListUserJL().setModel(m);
            
        } catch (Exception ex) {
           
            JOptionPane.showMessageDialog(null, ex.getMessage(), "erreur", JOptionPane.ERROR_MESSAGE);
        }
        
        frame.setVisible(true);
              
        
        
    }
    
     public boolean AddCollaborateurAction(ArrayList<user> users , Projet p , String date){
        boolean r=true;
        try {
            partageDAO model=new partageDAO();
            r= model.Ajout(users, p, date);
        } catch (Exception ex) {
            r=false;
            JOptionPane.showMessageDialog(null, ex.getMessage(), "erreur", JOptionPane.ERROR_MESSAGE);
        }
        
        return r;
    }
}

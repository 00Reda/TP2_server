/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import beans.ServerBeans.user;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.table.DefaultTableModel;
import model.userDAO;
import view.DashboardFrame;
import view.DashboardFrame;
import view.user.LoginFrame;

/**
 *
 * @author IdeaPad
 */
public class userController {
     
    
    public void loginAction(LoginFrame login){
        
        String s=String.valueOf( login.getjPasswordField().getPassword());
       user u=new user(login.getUsernameJtext().getText(),s,"admin");
       
        try {
              userDAO model=new userDAO();
              boolean r=model.login(u);
              if(r){
                   login.getErrorJlabel().setForeground(Color.green);
                   login.getErrorJlabel().setText("bienvenue .");
                   login.setVisible(false);
                   //System.out.println("view.Login.loginJbuttonActionPerformed()");
                   this.DashBoardAction();
                   
              }else{
                   login.getErrorJlabel().setText("password ou username invalide .");
              }
        } catch (Exception e) {
            login.getErrorJlabel().setText(e.getMessage());
        }
       
        
    }

    public void DashBoardAction() {
        
        DashboardFrame df= new DashboardFrame();
        DefaultTableModel tableModel=new DefaultTableModel(user.getClassProprties(), 0);
     
        df.getjTableUser().setModel(tableModel);
        try {
            
            userDAO ud=new userDAO();
            ArrayList<user> listes =ud.Lister();
            for (int i = 0; i < listes.size(); i++) {
                tableModel.addRow(listes.get(i).ObjectToVector());
            }
           
           
        } catch (Exception e) {
            JTextArea console=df.getConsoleArea();
            console.setText(console.getText()+"\n"+e.getMessage());
        }
        
        
        
        df.setVisible(true);
    }
    
    public boolean AddUserAction(user u){
        boolean r=true;
        try {
            userDAO model=new userDAO();
            r= model.Ajout(u);
        } catch (Exception ex) {
            r=false;
            JOptionPane.showMessageDialog(null, ex.getMessage(), "erreur", JOptionPane.ERROR_MESSAGE);
        }
        
        return r;
    }
    public boolean EditUserAction(user u , String old_username ){
        boolean r=true;
        try {
            userDAO model=new userDAO();
            r= model.Modification(u,old_username);
        } catch (Exception ex) {
            r=false;
            JOptionPane.showMessageDialog(null, ex.getMessage(), "erreur", JOptionPane.ERROR_MESSAGE);
        }
        
        return r;
    }
    
    public boolean DeleteUserAction(user u){
        boolean r=true;
        try {
            userDAO model=new userDAO();
            r= model.Suppression(u);
        } catch (Exception ex) {
            r=false;
            JOptionPane.showMessageDialog(null, ex.getMessage(), "erreur", JOptionPane.ERROR_MESSAGE);
        }
        
        return r;
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp2_server;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;
import services.userServices;
import view.user.LoginFrame;

/**
 *
 * @author IdeaPad
 */
public class TP2_server {
     
    public final static String HOST="localhost";
    public final static String PORT="1099";
  
    /**
     * @param args the command line arguments
     */
    public static void ALL_SERVICES_LANCHER(userServices UserServ){
        /**
         *  user services lancher .
         */
        
        
        try {
            LocateRegistry.createRegistry(Integer.valueOf(PORT));
            Naming.rebind("rmi://"+TP2_server.HOST+":"+TP2_server.PORT+"/US", UserServ);
            System.out.println(UserServ.toString());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        /**
         * 
         * 
         * 
         */
        
    }
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LoginFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LoginFrame().setVisible(true);
            }
        });
        
        /**
         *  publication des references des objets distrubués 
         */
        
       
        
        
        
                
    }
    
}

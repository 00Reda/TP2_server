/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp2_server;

import beans.ServerBeans.user;
import beans.forme;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;

/**
 *
 * @author IdeaPad
 */
public class ClientListnerThreadClass extends Thread{
    
    private static forme ClientForm=null;
    private static JTextArea console;
    private user Client;

    public ClientListnerThreadClass(JTextArea jt,user u) {
        console=jt;
        this.Client=u;
    }

    public synchronized user getClient() {
        return Client;
    }

    public synchronized void setClient(user Client) {
        this.Client = Client;
    }

    
    
    
    public synchronized static JTextArea getConsole() {
        return console;
    }

    public synchronized static void setConsole(JTextArea console) {
        ClientListnerThreadClass.console = console;
    }

    
    
    public synchronized static forme getClientForm() {
        return ClientForm;
    }

    public synchronized static void setClientForm(forme ClientForm) {
        ClientListnerThreadClass.ClientForm = ClientForm;
    }
   
    
    
    @Override
    public void run() {
        
        getConsole().setText(getConsole().getText()+"\n"+"nouveau utilisateur connecte : "+this.getClient().getUsername());
        
        
        while(true){
            try {
                sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(ClientListnerThreadClass.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println("user connected while true : "+this.getClient().getUsername());
        }
        
    }
    
}

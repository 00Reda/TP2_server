/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view.user;

import beans.ServerBeans.user;
import controllers.userController;
import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import view.DashboardFrame;

/**
 *
 * @author IdeaPad
 */
public class EditUserFrame extends javax.swing.JFrame {

    /**
     * Creates new form EditUserFrame
     */
    
   private DashboardFrame parent ;
   private  String old_username ;
   private user userToEdit;
   private int index ;
    public EditUserFrame(DashboardFrame parent ,user u ,int index) {
        this.parent=parent;
        this.index=index;
        initComponents();
        Dimension PC=this.getToolkit().getScreenSize();
        this.setLocation((PC.width-this.getWidth())/2,(PC.height-this.getHeight())/2);
       
        this.addWindowListener(new WindowAdapter() {
           
            public void windowClosing(WindowEvent e)
            {
                e.getWindow().dispose();
            }
            
        });
        this.userToEdit=u;
        this.old_username=u.getUsername();
        this.usernameJT.setText(u.getUsername());
        this.emailJT.setText(u.getEmail());
      
        
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel6 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        usernameJT = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        emailJT = new javax.swing.JTextField();
        ConfirmPasswordJP = new javax.swing.JPasswordField();
        passwordJP = new javax.swing.JPasswordField();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/res/add-user-button.png"))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel1.setText("Modifier un utilisateur :");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Username : ");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Email :");

        usernameJT.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        usernameJT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                usernameJTActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel4.setText("Retaper Password :");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setText("Password :");

        emailJT.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        emailJT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                emailJTActionPerformed(evt);
            }
        });

        ConfirmPasswordJP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ConfirmPasswordJPActionPerformed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jButton1.setText("Modifier");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(passwordJP, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(emailJT)
                                .addComponent(usernameJT)
                                .addComponent(ConfirmPasswordJP, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING)))
                    .addComponent(jButton1))
                .addContainerGap(93, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(43, 43, 43)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel1)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addContainerGap(323, Short.MAX_VALUE)))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(157, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(usernameJT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addGap(13, 13, 13)
                .addComponent(emailJT, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(passwordJP, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ConfirmPasswordJP, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButton1)
                .addGap(29, 29, 29))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, 92, Short.MAX_VALUE)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(jLabel1)
                    .addGap(361, 361, 361)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void usernameJTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_usernameJTActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_usernameJTActionPerformed

    private void emailJTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_emailJTActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_emailJTActionPerformed

    private void ConfirmPasswordJPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ConfirmPasswordJPActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ConfirmPasswordJPActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:

        String uname=this.usernameJT.getText();
        String upwd=String.copyValueOf(this.passwordJP.getPassword());
        String cupwd=String.copyValueOf(this.ConfirmPasswordJP.getPassword());
        String email=this.emailJT.getText();
        boolean r=true;
        
        if(uname.equals("")){
            r=false;
            JOptionPane.showMessageDialog(this, "entre un username valide", "erreur", JOptionPane.ERROR_MESSAGE);
        }else{
            this.userToEdit.setUsername(uname);
        }
        if(!upwd.equals("") && upwd.equals(cupwd)){
            this.userToEdit.setPassword(tools.Security.getCryptedPassword(upwd));
        }
        if(email.equals("")){
            r=false;
            JOptionPane.showMessageDialog(this, "entre un email valide", "erreur", JOptionPane.ERROR_MESSAGE);
        }else{
            this.userToEdit.setEmail(email);
        }
        
        if(!r) return ;
       
        userController uc= new userController();
        if(uc.EditUserAction(this.userToEdit,this.old_username)==true){
            DefaultTableModel model= (DefaultTableModel) this.parent.getjTableUser().getModel();
            model.setValueAt(this.userToEdit.getUsername(), this.index, 0);
            model.setValueAt(this.userToEdit.getPassword(), this.index, 1);
            model.setValueAt(this.userToEdit.getEmail(), this.index, 2);
            model.setValueAt(this.userToEdit.getRole(), this.index, 3);
            model.setValueAt(this.userToEdit.getDate_ajout(), this.index, 4);
            this.parent.getConsoleArea().setText(this.parent.getConsoleArea().getText()+"\n"+"> user modifié : nom = "+userToEdit.getUsername());
            this.dispose();
            JOptionPane.showMessageDialog(this, "user modifié", "ajout user", JOptionPane.INFORMATION_MESSAGE);
        }else{
            JOptionPane.showMessageDialog(this, "erreur ressayer !!", "modif user erreur", JOptionPane.INFORMATION_MESSAGE);
        }

    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
   

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPasswordField ConfirmPasswordJP;
    private javax.swing.JTextField emailJT;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPasswordField passwordJP;
    private javax.swing.JTextField usernameJT;
    // End of variables declaration//GEN-END:variables
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import beans.ServerBeans.Projet;
import beans.ServerBeans.partage;
import beans.ServerBeans.user;
import beans.forme;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import services.ClientServices.IClientServices;

/**
 *
 * @author IdeaPad
 */
public interface IuserServicesRemote extends Remote{
    
      /**
       * 
       * @param u
       * @return
       * @throws RemoteException 
       */
      public String login (user u , IClientServices notify) throws RemoteException;
      /**
       * 
       * @param u
       * @return
       * @throws RemoteException 
       */
      public boolean disconnect (user u) throws RemoteException;
      ///////////////////////////////////////////////////////////////
      
      /**
       * 
       * @param p
       * @param u
       * @return
       * @throws RemoteException 
       */
      public boolean AddProjet(Projet p) throws RemoteException ;
      /**
       * 
       * @param u
       * @return
       * @throws RemoteException 
       */
      public ArrayList<Projet> GetMesProjet (user u) throws RemoteException;
      /**
       * 
       * @param u
       * @return
       * @throws RemoteException 
       */
      public ArrayList<Projet> GetProjetPartager (user u) throws RemoteException;
      /**
       * 
       * @param p
       * @return
       * @throws RemoteException 
       */
      public ArrayList<forme> GetProjetFormes (Projet p, user u) throws RemoteException;
      /**
       * 
       * @param p
       * @return
       * @throws RemoteException 
       */
      public boolean DeleteProjet(Projet p ) throws RemoteException;
      
       ////////////////////////////////////////////////////////////////////////////////
      
      
      /**
       * 
       * @param p
       * @return
       * @throws RemoteException 
       */
      
      public ArrayList<partage> ListCollaborateurProjet(Projet p ) throws RemoteException;
      /**
       * 
       * @param p
       * @return
       * @throws RemoteException 
       */
      public ArrayList<user> GetListOfNonCollaboratedUsers(Projet p) throws RemoteException ;
      /**
       * 
       * @param p
       * @param listUser
       * @return
       * @throws RemoteException 
       */
      public boolean AddCollaborateur(Projet p , ArrayList<user> listUser) throws RemoteException;
      
      ////////////////////////////////////////////////////////////////////////////////////////
      
      public long addForme(forme f,Projet p,user u) throws RemoteException;
      public boolean modifyForme(forme f, Projet p,user u) throws RemoteException;
      public boolean deleteForme(forme f,Projet p,user u) throws RemoteException;
}

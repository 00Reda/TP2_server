/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import beans.ServerBeans.Projet;
import beans.ServerBeans.partage;
import beans.ServerBeans.user;
import beans.forme;
import java.rmi.RemoteException;
 
import java.rmi.server.UnicastRemoteObject;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.JTextArea;
import model.partageDAO;
import model.projetDAO;
import model.userDAO;
import services.ClientServices.IClientServices;

/**
 *
 * @author IdeaPad
 */
public class userServices extends UnicastRemoteObject implements IuserServicesRemote{

    private JTextArea console ;
    private ArrayList<user> ConnectedUserList=new ArrayList<>();
    private ArrayList<VisualisationProjet> VisualisationSyncList =new ArrayList<>();
    
    public userServices(JTextArea console) throws RemoteException {
        super();
        
        this.console=console;
    }

    public synchronized JTextArea getConsole() {
        return console;
    }

    public synchronized void setConsole(JTextArea console) {
        this.console = console;
    }
    
    

    @Override
    public String login(user u,IClientServices notify) throws RemoteException {
        String msg="";
        System.out.println(u);
        try {
                if(this.ConnectedUserList.contains(u)){
                    return "vous etes deja connecté !! ";
                }
                userDAO m=new userDAO();
                if(m.login(u)){
                    /*
                    ClientListnerThreadClass c=new ClientListnerThreadClass(this.console,u);
                    c.start();
                     */
                    console.setText(getConsole().getText()+"\n"+"nouveau utilisateur connecte : "+u.getUsername());
                    this.ConnectedUserList.add(u);
                    msg= "bienvenu cher client ";
                    
                    this.VisualisationSyncList.add(new VisualisationProjet(u, null, notify));
                    
                }else{
                    msg="invalide username ou password : ressayer svp ";
                }
        } catch (Exception e) {
            return e.getMessage();
        }
        return msg;
    }
    @Override
    public boolean disconnect(user u) throws RemoteException {
       
        if(this.ConnectedUserList.contains(u)){
            
            this.ConnectedUserList.remove(u);
            console.setText(getConsole().getText()+"\n"+" utilisateur deconnecté : "+u.getUsername());
            return true;
        }else{
            return false;
        }
    }
    @Override
    public ArrayList<Projet> GetMesProjet(user u) throws RemoteException {
        projetDAO model;
        try {
            model = new projetDAO();
            return model.Lister(u.getUsername());
        } catch (Exception ex) {
            return null;
        }
        
      
    }

    @Override
    public ArrayList<Projet> GetProjetPartager(user u) throws RemoteException {
        projetDAO model;
        try {
            model = new projetDAO();
            return model.getSharedProjet(u);
        } catch (Exception ex) {
            return null;
        }
    }

    @Override
    public boolean DeleteProjet(Projet p) throws RemoteException {
        
        projetDAO model;
        try {
            model = new projetDAO();
            return model.Suppression(p);
        } catch (Exception ex) {
           return false;
        }
        
    }

    @Override
    public ArrayList<partage> ListCollaborateurProjet(Projet p) throws RemoteException {
        
        partageDAO model;
        try {
            model = new partageDAO();
           return model.Lister(p);
        } catch (Exception ex) {
           return null;
        }
        
    }

    @Override
    public ArrayList<user> GetListOfNonCollaboratedUsers(Projet p) throws RemoteException {
        partageDAO model;
        try {
            model = new partageDAO();
           return model.getUserNoCollaborer(p);
        } catch (Exception ex) {
           return null;
        }
    }

    @Override
    public boolean AddCollaborateur(Projet p, ArrayList<user> listUser) throws RemoteException {
         partageDAO model;
        try {
            model = new partageDAO();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
            String date = sdf.format(new Date());
            return model.Ajout(listUser, p, date);
        } catch (Exception ex) {
           return false;
        }
    }

    @Override
    public ArrayList<forme> GetProjetFormes(Projet p , user u) throws RemoteException {
        
        projetDAO model;
        try {
            model = new projetDAO();
           ArrayList<forme> lf= model.GetProjectForms(p);
         
            for (VisualisationProjet vs : this.VisualisationSyncList) {
                if(vs.getUser().getUsername().equals(u.getUsername())){
                    vs.setProjet(p);
                    break;
                }
            }
            
           return lf;
        } catch (Exception ex) {
            ex.printStackTrace();
           return null;
        }
        
    }

    @Override
    public boolean AddProjet(Projet p) throws RemoteException {
        
         projetDAO model;
        try {
            model = new projetDAO();
            return model.Ajout(p);
        } catch (Exception ex) {
             ex.printStackTrace();
           return false;
        }
    }

    @Override
    public long addForme(forme f , Projet p , user u) throws RemoteException {
         projetDAO model;
        try {
            model = new projetDAO();
            long id=model.AddForm(p, f);
            System.out.println(f);
            System.out.println(id);
            if(id!=-1){
                f.setID(id);
              
                for (VisualisationProjet vs : VisualisationSyncList ) {
                    if(vs.getProjet().getId_projet()==p.getId_projet() && vs.getUser().getUsername().equals(u.getUsername())==false){
                        
                        vs.getNotify().ReseiveNewForm(f);
                    }
                }
                
                
                return id;
            }
            return -1;
        } catch (Exception ex) {
            ex.printStackTrace();
           return -1;
        }
    }

    @Override
    public boolean modifyForme(forme f, Projet p,user u) throws RemoteException {
         projetDAO model;
        try {
            model = new projetDAO();
            boolean res= model.ModifyForm(f);
            if(res){
                 for (VisualisationProjet vs : VisualisationSyncList) {
                   //  && vs.getUser().getUsername().equals(u.getUsername())==false
                    if(vs.getProjet().getId_projet()==p.getId_projet() ){
                                              
                        vs.getNotify().ModifyForm(f);
                    }
                }
                return res;
            }else{
                return false;
            }
        } catch (Exception ex) {
             ex.printStackTrace();
           return false;
        }
    }

    @Override
    public boolean deleteForme(forme f, Projet p,user u) throws RemoteException {
         projetDAO model;
        try {
            model = new projetDAO();
            boolean res= model.DeleteForm(f);
            System.out.println(res+":"+f);
            if(res){
                for (VisualisationProjet vs : VisualisationSyncList) {
                    if(vs.getProjet().getId_projet()==p.getId_projet()){
                      
                        vs.getNotify().DeleteForm(f);
                    }
                }
                
               return true; 
            }else return false;
            
        } catch (Exception ex) {
             ex.printStackTrace();
           return false;
        }
    }

   
    
}

package services;

import beans.ServerBeans.Projet;
import beans.ServerBeans.user;
import services.ClientServices.IClientServices;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author IdeaPad
 */
public class VisualisationProjet {
    
    private user User;
    private Projet Projet ;
    private IClientServices notify ;

    public VisualisationProjet(user User, Projet Projet, IClientServices notify) {
        this.User = User;
        this.Projet = Projet;
        this.notify = notify;
    }

    public user getUser() {
        return User;
    }

    public void setUser(user User) {
        this.User = User;
    }

    public Projet getProjet() {
        return Projet;
    }

    public void setProjet(Projet Projet) {
        this.Projet = Projet;
    }

    public IClientServices getNotify() {
        return notify;
    }

    public void setNotify(IClientServices notify) {
        this.notify = notify;
    }
            

    
    
    
    
}

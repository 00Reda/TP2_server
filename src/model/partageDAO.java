/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;
import beans.ServerBeans.Projet;
import beans.ServerBeans.partage;
import beans.ServerBeans.user;
import java.util.ArrayList;
/**
 *
 * @author IdeaPad
 */
public class partageDAO extends DAO<partage>{

    public partageDAO() throws Exception {
    }
    public boolean Ajout(ArrayList<user> users , Projet p , String date) throws Exception {
        
        StringBuffer requete=new StringBuffer("INSERT INTO partage ( id_projet , username , date_partage ) VALUES ");
        for (int i=0 ; i<users.size();i++) {
            requete.append("("+p.getId_projet()+", '"+users.get(i).getUsername()+"', '"+date+"' ) ");
            
            if(i!=users.size()-1){
                requete.append(",");
            }
        }
        requete.append(";");
        //System.out.println(requete);
        this.stm=this.cnx.createStatement();
        int nbr=this.stm.executeUpdate(requete.toString());
        if(nbr>=1){
            return true;
        }else{
            return false;
        }
    }
    @Override
    public boolean Ajout(partage data) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean Suppression(partage data) throws Exception {
       boolean r=true;
         String requete="delete from partage where id_projet="+data.getP().getId_projet()+" and username = '"+data.getUsers().getUsername()+"' ;";
         this.stm=this.cnx.createStatement();
         int nbr = this.stm.executeUpdate(requete);
         if(nbr<1) r=false;
         
         return r;
    }

    @Override
    public boolean Modification(partage data) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public ArrayList<partage> Lister(Projet p) throws Exception {
           
         String requete="SELECT * FROM partage, user WHERE partage.username=user.username and id_projet= "+p.getId_projet()+" ;";
         
         this.stm=this.cnx.createStatement();
         this.stm.executeQuery(requete);
         this.rs=this.stm.getResultSet();
         ArrayList liste=new ArrayList();
         while (rs.next()) {
            user user =new user(rs.getString("username"), rs.getString("password"), rs.getString("email"), rs.getString("role"), rs.getString("date_ajout"));
            partage pr=new partage(user,rs.getString("date_partage") , p);
            liste.add(pr);
         }
         
         return liste;  
    }

    @Override
    public ArrayList<partage> Lister() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public ArrayList<user> getUserNoCollaborer(Projet p) throws Exception{
         String requete=" SELECT * FROM user WHERE role='user' and username != '"+p.getOwner_projet()+"' and username not in (select username from partage where id_projet="+p.getId_projet()+" ) ; ";
         //System.out.println(requete);
         
         this.stm=this.cnx.createStatement();
         this.stm.executeQuery(requete);
         this.rs=this.stm.getResultSet();
         ArrayList liste=new ArrayList();
         while (rs.next()) {
            user user =new user(rs.getString("username"), "", rs.getString("email"), "", "" );
            liste.add(user);
         }
         
         return liste;  
    }
            
    
    
}

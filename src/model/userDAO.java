/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import beans.ServerBeans.user;
import java.util.ArrayList;

/**
 *
 * @author IdeaPad
 */
public class userDAO extends DAO<user>{

    public userDAO() throws Exception {
        super();
    } 
   
    public boolean login (user u) throws Exception{
        
         boolean r=false;
         String requete="select * from user where username= ? and password= ? and role= '"+u.getRole()+"';";
         this.preparedStm=this.cnx.prepareStatement(requete);
         this.preparedStm.setString(1, u.getUsername());
         this.preparedStm.setString(2, u.getPassword());
         // System.out.println(requete);
         this.rs=this.preparedStm.executeQuery();
         if(rs.next()==false){
             
            return r;
            
         }else{
               
            return true;
         }
         
        
    }
    
    @Override
    public boolean Ajout(user data) throws Exception {
         boolean r=true;
        
         String requete="select * from user where username= ? ;";
         this.preparedStm=this.cnx.prepareStatement(requete);
         this.preparedStm.setString(1, data.getUsername());
         this.rs=this.preparedStm.executeQuery();
         
         if(rs.next()==true){
             
            throw new Exception("utilisateur deja exist !!");
            
         }else{
        
          requete="INSERT INTO user (`username`, `password`, `email`, `date_ajout`, `role`) "
                        + "VALUES ('"+data.getUsername()+"', '"+data.getPassword()+"', '"+data.getEmail()+"', '"+data.getDate_ajout()+"', 'user');";
         this.stm=this.cnx.createStatement();
         int nbr = this.stm.executeUpdate(requete);
         if(nbr<1) r=false;
         
         return r;
         }
    }

  

    @Override
    public boolean Suppression(user data) throws Exception{
        boolean r=true;
        String requete="delete from user where username='"+data.getUsername()+"' ;";
         this.stm=this.cnx.createStatement();
         int nbr = this.stm.executeUpdate(requete);
         if(nbr<1) r=false;
         
         return r;
    }

    public boolean Modification(user data , String old_username) throws Exception{
        
        boolean r=true;
        
         if(!old_username.equals(data.getUsername())){
             
                String q="select * from user where username= ? ;";
                this.preparedStm=this.cnx.prepareStatement(q);
                this.preparedStm.setString(1, data.getUsername());
                this.rs=this.preparedStm.executeQuery();
         
                if(rs.next()==true){
                   
                   throw new Exception("utilisateur deja exist !!");
                   
                }
         }
       
        
         String requete="UPDATE user SET "
                  + "username = '"+data.getUsername()+"',"
                  + "password = '"+data.getPassword()+"',"
                  + " email = '"+data.getEmail()+"' "
                  + " WHERE  username = '"+old_username+"';";
            // System.out.println(requete);
         this.stm=this.cnx.createStatement();
         int nbr = this.stm.executeUpdate(requete);
         if(nbr<1) r=false;
         
         return r;
         
         
    }
    
    @Override
    public ArrayList<user> Lister() throws Exception{
         String requete="select * from user ;";
         this.stm=this.cnx.createStatement();
         this.stm.executeQuery(requete);
         this.rs=this.stm.getResultSet();
         ArrayList liste=new ArrayList();
         while (rs.next()) {            
            user u =new user(rs.getString("username"), rs.getString("password"), rs.getString("email"), rs.getString("role"), rs.getString("date_ajout"));
            liste.add(u);
         }
         
         return liste;
    }

    @Override
    public boolean Modification(user data) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    
}

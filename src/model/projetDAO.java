/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import beans.ServerBeans.Projet;
import beans.ServerBeans.user;
import beans.caree;
import beans.circle;
import beans.disque;
import beans.ellipse;
import beans.forme;
import beans.ligne;
import beans.polyLigne;
import beans.rectangle;
import java.awt.Color;
import java.awt.Point;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import tools.ColorEncode;
import tools.PointStorage;

/**
 *
 * @author IdeaPad
 */
public class projetDAO extends DAO<Projet>{

    public projetDAO() throws Exception {
        super();
    }

    
    public boolean Ajout(Projet p) throws Exception {
      boolean r=true;
      String  requete="INSERT INTO projet (id_projet, nom_projet, date_projet, owner_projet, background_projet)"
                + " VALUES (NULL, '"+p.getNom_projet()+"', '"+p.getDate_projet()+"', '"+p.getOwner_projet()+"', '"+p.getBackground_projet()+"');";
       this.stm=this.cnx.createStatement();
       int nbr = this.stm.executeUpdate(requete);
       if(nbr<1) r=false;
         
         return r;
    }

    @Override
    public boolean Suppression(Projet data) throws Exception {
         boolean r=true;
         String requete="delete from projet where id_projet="+data.getId_projet()+" ;";
         this.stm=this.cnx.createStatement();
         int nbr = this.stm.executeUpdate(requete);
         if(nbr<1) r=false;
         
         return r;
    }

    @Override
    public boolean Modification(Projet data) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<Projet> Lister() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public ArrayList<Projet> Lister(String username) throws Exception {
        
        String requete="select * from projet where owner_projet='"+username+"' ;";
         this.stm=this.cnx.createStatement();
         this.stm.executeQuery(requete);
         this.rs=this.stm.getResultSet();
         ArrayList liste=new ArrayList();
         while (rs.next()) {            
            Projet u =new Projet(rs.getInt("id_projet"),rs.getString("nom_projet"),rs.getString("date_projet"),rs.getString("owner_projet"),rs.getString("background_projet"));
            liste.add(u);
         }
         
         return liste;
    }
    
    public ArrayList<forme> GetProjectForms(Projet p) throws Exception{
        
        ArrayList <forme> f=new ArrayList<forme>();
        String requete ;
        
        requete="select * from caree where id_projet="+p.getId_projet()+" ;";
        
        this.stm=this.cnx.createStatement();
        this.stm.executeQuery(requete);
        this.rs=this.stm.getResultSet();
        while(this.rs.next()){
            caree c=new caree(PointStorage.FromStringToPoint(this.rs.getString("point_begin")), PointStorage.FromStringToPoint(this.rs.getString("point_end")),Color.decode(this.rs.getString("color")), this.rs.getInt("epaisseur_caree"), this.rs.getInt("id_caree"));
            f.add(c);
        }
        
        requete="select * from cercle where id_projet="+p.getId_projet()+" ;";
        
        this.stm=this.cnx.createStatement();
        this.stm.executeQuery(requete);
        this.rs=this.stm.getResultSet();
       
        while(this.rs.next()){
            circle c=new circle(PointStorage.FromStringToPoint(this.rs.getString("centre")),  this.rs.getInt("rayon"),Color.decode(rs.getString("color_cercle")), rs.getInt("epaisseur_cercle"),rs.getInt("id_cercle") );
            f.add(c);
        }
        
        requete="select * from disque where id_projet="+p.getId_projet()+" ;";
        
        this.stm=this.cnx.createStatement();
        this.stm.executeQuery(requete);
        this.rs=this.stm.getResultSet();
       
        while(this.rs.next()){
            disque c=new disque(PointStorage.FromStringToPoint(this.rs.getString("centre_disque")), rs.getInt("rayon_disque"),Color.decode(rs.getString("color_disque")), Color.blue, rs.getInt("epaisseur_disque"), rs.getInt("id_disque"));
           
            f.add(c);
        }
        
        requete="select * from ellipse where id_projet="+p.getId_projet()+" ;";
        
        this.stm=this.cnx.createStatement();
        this.stm.executeQuery(requete);
        this.rs=this.stm.getResultSet();
       
        while(this.rs.next()){
            ellipse c=new ellipse(PointStorage.FromStringToPoint(this.rs.getString("centre_ellipse")), rs.getInt("hauteur"),rs.getInt("largueur"), Color.decode(rs.getString("color_ellipse")),rs.getInt("epaisseur_ellipse"), rs.getInt("id_ellipse"));
            f.add(c);
        }
        
        requete="select * from ligne where id_projet="+p.getId_projet()+" ;";
        
        this.stm=this.cnx.createStatement();
        this.stm.executeQuery(requete);
        this.rs=this.stm.getResultSet();
       
        while(this.rs.next()){
            ligne l=new ligne(PointStorage.FromStringToPoint(this.rs.getString("point_begin")), PointStorage.FromStringToPoint(this.rs.getString("point_end")), Color.decode(rs.getString("color")),rs.getInt("epaisseur"), rs.getInt("id_ligne"));
            f.add(l);
        }
        
        requete="select * from polyligne where id_projet="+p.getId_projet()+" ;";
        
        this.stm=this.cnx.createStatement();
        this.stm.executeQuery(requete);
        this.rs=this.stm.getResultSet();
       
        while(this.rs.next()){
            polyLigne l=new polyLigne(Color.decode(rs.getString("color_polyligne")), rs.getInt("epaisseur_polyligne"), PointStorage.StringToPolyline(rs.getString("point_liste")), rs.getInt("id_polyligne"));
            f.add(l);
        }
        
        requete="select * from rectangle where id_projet="+p.getId_projet()+" ;";
        
        this.stm=this.cnx.createStatement();
        this.stm.executeQuery(requete);
        this.rs=this.stm.getResultSet();
       
        while(this.rs.next()){
            Point p1=PointStorage.FromStringToPoint(rs.getString("point1_rectangle"));
            Point p2=PointStorage.FromStringToPoint(rs.getString("point2_rectangle"));
            rectangle l=new  rectangle(p1, p2, Color.decode(rs.getString("color_rectangle")), rs.getInt("epaisseur_rectangle"), rs.getInt("id_rectangle"));
            f.add(l);
        }
         
        return f;
    }
    
    public long AddForm(Projet p , forme f) throws Exception{
        
        
             String requete="";
        
            
            if(f instanceof caree) {
                caree c=(caree) f;
                requete="INSERT INTO caree (epaisseur_caree, id_caree, id_projet, point_begin, point_end, color) "
                        + "VALUES ("+c.getEpaisseur()+", NULL,"+p.getId_projet()+" , '"+PointStorage.FromPointToString(c.getP1())+"', '"+PointStorage.FromPointToString(c.getP2())+"', '" +ColorEncode.GetHex(c.getCouleur()) +"');";
                 
            } else if(f instanceof circle){
                circle c=(circle) f;
                requete="INSERT INTO cercle (color_cercle, epaisseur_cercle, id_cercle, id_projet, centre, rayon)"
                        + " VALUES ('"+ColorEncode.GetHex(f.getCouleur())+"', "+f.getEpaisseur()+", NULL, "+p.getId_projet()+", '"+PointStorage.FromPointToString(c.getCenter())+"', "+c.getR()+");";
            }else if(f instanceof disque){
                disque c=(disque) f;
                requete="INSERT INTO disque (color_disque, epaisseur_disque, id_disque, id_projet, centre_disque, rayon_disque) "
                        + "VALUES ('"+ColorEncode.GetHex(c.getCouleur())+"', "+f.getEpaisseur()+", NULL,"+p.getId_projet()+", '"+PointStorage.FromPointToString(c.getCenter())+"', '"+c.getR()+"');";
                
            }else if(f instanceof ellipse){
                ellipse c=(ellipse) f;
                requete="INSERT INTO ellipse (color_ellipse, epaisseur_ellipse, id_ellipse, id_projet, centre_ellipse, largueur, hauteur) "
                        + "VALUES ('"+ColorEncode.GetHex(c.getCouleur())+"', "+f.getEpaisseur()+", NULL, "+p.getId_projet()+", '"+PointStorage.FromPointToString(c.getCenter())+"', "+c.getWidth()+", "+c.getHeight()+");";
            }
            else if(f instanceof ligne){
                ligne c=(ligne) f;
                requete="INSERT INTO ligne (color, epaisseur, id_ligne, id_projet, point_begin, point_end) "
                        + "VALUES ('"+ColorEncode.GetHex(c.getCouleur())+"',"+c.getEpaisseur()+", NULL, "+p.getId_projet()+", '"+PointStorage.FromPointToString(c.getBegin())+"', '"+PointStorage.FromPointToString(c.getEnd())+"');";
            }
             else if(f instanceof polyLigne){
                polyLigne c=(polyLigne) f;
                requete="INSERT INTO polyligne (color_polyligne, epaisseur_polyligne, id_polyligne, id_projet, point_liste)"
                        + " VALUES ('"+ColorEncode.GetHex(c.getCouleur())+"', "+c.getEpaisseur()+", NULL, "+p.getId_projet()+", '"+PointStorage.PolylineToString(c.getListPoint())+"');";
            }else if(f instanceof rectangle){
                rectangle c=(rectangle) f;
                requete="INSERT INTO rectangle (color_rectangle, epaisseur_rectangle, id_rectangle, id_projet, point1_rectangle, point2_rectangle)"
                        + " VALUES ('"+ColorEncode.GetHex(c.getCouleur())+"',  "+c.getEpaisseur()+", NULL, "+p.getId_projet()+", '"+PointStorage.FromPointToString(c.getP1())+"', '"+PointStorage.FromPointToString(c.getP2())+"');";
                        
            }
            
            this.stm=this.cnx.createStatement();
            int nbr = this.stm.executeUpdate(requete, Statement.RETURN_GENERATED_KEYS);
            long id=-1;
            if(nbr<1) return -1;
            else{
                this.rs=this.stm.getGeneratedKeys();
            
            
                if(rs.next()){
                    id=rs.getInt(1);
                }
            }
            
           return id;
    }
    
    
    public boolean ModifyForm(forme f) throws Exception{
        
        String requete="";
        
            
            if(f instanceof caree) {
                caree c=(caree) f;
                 requete="UPDATE caree "
                         + "SET "
                         + "epaisseur_caree = "+c.getEpaisseur()+","
                         + " point_begin ='"+PointStorage.FromPointToString(c.getP1())+"', "
                         + "point_end = '"+PointStorage.FromPointToString(c.getP2())+"', "
                         + "color ='" +ColorEncode.GetHex(c.getCouleur()) +"' "
                         + "WHERE id_caree = "+c.getId_caree()+";";
            } else if(f instanceof circle){
                circle c=(circle) f;
                requete="UPDATE cercle"
                        + " SET "
                        + "color_cercle = '" +ColorEncode.GetHex(c.getCouleur()) +"', "
                        + "epaisseur_cercle ="+c.getEpaisseur()+", "
                        + "centre = '"+PointStorage.FromPointToString(c.getCenter())+"',"
                        + " rayon = "+c.getR()+" "
                        + "WHERE id_cercle = "+c.getId_circle()+";";
            }else if(f instanceof disque){
                disque c=(disque) f;
                requete="UPDATE disque"
                        + " SET "
                        + "color_disque ='" +ColorEncode.GetHex(c.getCouleur()) +"',"
                        + " epaisseur_disque = "+c.getEpaisseur()+", "
                        + "centre_disque = '"+PointStorage.FromPointToString(c.getCenter())+"' "
                        + "rayon_disque = "+c.getR()+""
                        + " WHERE id_disque = "+c.getId_disque()+";";
            }else if(f instanceof ellipse){
                ellipse c=(ellipse) f;
                requete="UPDATE ellipse "
                        + "SET "
                        + "color_ellipse = '" +ColorEncode.GetHex(c.getCouleur()) +"'"
                        + ", epaisseur_ellipse ="+c.getEpaisseur()+", "
                        + "centre_ellipse = '"+PointStorage.FromPointToString(c.getCenter())+"', "
                        + "largueur = "+c.getWidth()+", "
                        + "hauteur ="+c.getHeight()+""
                        + " WHERE id_ellipse ="+c.getId_ellipse()+" ;";
            }
            else if(f instanceof ligne){
                ligne c=(ligne) f;
                requete="UPDATE ligne"
                        + " SET"
                        + "color = '" +ColorEncode.GetHex(c.getCouleur()) +"', "
                        + "epaisseur ="+c.getEpaisseur()+", "
                        + "point_begin ='"+PointStorage.FromPointToString(c.getBegin())+"',"
                        + " point_end = '"+PointStorage.FromPointToString(c.getEnd())+"'"
                        + " WHERE id_ligne = "+c.getId_ligne()+";";
            }
             else if(f instanceof polyLigne){
                polyLigne c=(polyLigne) f;
                requete="UPDATE polyligne "
                        + "SET"
                        + " color_polyligne ='" +ColorEncode.GetHex(c.getCouleur()) +"', "
                        + "epaisseur_polyligne ="+c.getEpaisseur()+", "
                        + "point_liste ='"+PointStorage.PolylineToString(c.getListPoint())+"'"
                        + " WHERE id_polyligne = "+c.getId_polyligne()+";";
            }else if(f instanceof rectangle){
                rectangle c=(rectangle) f;
                requete="UPDATE rectangle"
                        + " SET "
                        + "color_rectangle ='" +ColorEncode.GetHex(c.getCouleur()) +"',"
                        + " epaisseur_rectangle = "+c.getEpaisseur()+","
                        + " point1_rectangle = '"+PointStorage.FromPointToString(c.getP1())+"', "
                        + "point2_rectangle = '"+PointStorage.FromPointToString(c.getP2())+"' "
                        + "WHERE id_rectangle= "+c.getId_rectangle()+";";
                        
            }
            
            this.stm=this.cnx.createStatement();
            int nbr = this.stm.executeUpdate(requete);
            if(nbr<1) return false;
        
           return true;
    }
    
    public boolean DeleteForm(forme f) throws Exception{
        
        
             String requete="";
        
            
            if(f instanceof caree) {
                caree c=(caree) f;
                  requete="DELETE FROM caree"
                          + " WHERE id_caree ="+c.getId_caree()+";";
            } else if(f instanceof circle){
                circle c=(circle) f;
                  requete="DELETE FROM cercle"
                          + " WHERE id_cercle ="+c.getId_circle()+";";
            }else if(f instanceof disque){
                disque c=(disque) f;
                  requete="DELETE FROM disque"
                          + " WHERE id_disque ="+c.getId_disque()+";";                
            }else if(f instanceof ellipse){
                ellipse c=(ellipse) f;
                  requete="DELETE FROM ellipse"
                          + " WHERE id_ellipse ="+c.getId_ellipse()+";";
            }
            else if(f instanceof ligne){
                ligne c=(ligne) f;
                  requete="DELETE FROM ligne"
                          + " WHERE id_ligne ="+c.getId_ligne()+";";
            }
             else if(f instanceof polyLigne){
                polyLigne c=(polyLigne) f;
                  requete="DELETE FROM polyligne"
                          + " WHERE id_polyligne ="+c.getId_polyligne()+";";
             }else if(f instanceof rectangle){
                rectangle c=(rectangle) f;
                  requete="DELETE FROM rectangle"
                          + " WHERE id_rectangle="+c.getId_rectangle()+";";
                        
            }
            
            this.stm=this.cnx.createStatement(); 
            int nbr = this.stm.executeUpdate(requete);
            if(nbr<1) return false;
        
           return true;
    }
     
    public ArrayList<Projet> getSharedProjet(user u) throws Exception{
        String requete="select projet.id_projet , projet.nom_projet , projet.background_projet , projet.owner_projet , projet.date_projet "
                + "    from projet , partage"
                + "    WHERE projet.id_projet=partage.id_projet "
                + "    and partage.username='"+u.getUsername()+"' ;";
        
         this.stm=this.cnx.createStatement();
         this.stm.executeQuery(requete);
         this.rs=this.stm.getResultSet();
         ArrayList liste=new ArrayList();
         while (rs.next()) {            
            Projet p =new Projet(rs.getInt("id_projet"),rs.getString("nom_projet"),rs.getString("date_projet"),rs.getString("owner_projet"),rs.getString("background_projet"));
            liste.add(p);
         }
        
        return liste;
    }
    
    public static void main(String [] args){
        
        
           projetDAO c;
        try {
            
            c = new projetDAO();
            ArrayList l=new ArrayList();
            l.add(new Point(0, 0));
            l.add(new Point(100, 100));
            l.add(new Point(200 ,200));
            
            polyLigne f= new polyLigne(Color.blue, 8,l ,0);
                   
           long t= c.AddForm(new Projet(1), f);
            System.out.println("model.projetDAO.main()"+ t);
            /*
           c = new projetDAO();
            ArrayList l=new ArrayList();
            l=c.GetProjectForms(new Projet(18));
            for (Iterator iterator = l.iterator(); iterator.hasNext();) {
                Projet next = (Projet) iterator.next();
                System.out.println(next);
            }
            */
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
           
        
        
    }

    
    
     
    
    
}

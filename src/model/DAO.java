/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

 
import java.sql.*;
import java.util.ArrayList;
 

/**
 *
 * @author IdeaPad
 */
public abstract class DAO<T> {
    
        protected Connection cnx=null;
	protected Statement stm;
        protected PreparedStatement preparedStm;
	protected ResultSet rs;
	
	
	 public DAO() throws Exception
        {
                 if(this.cnx==null){
                  
                            Class.forName("com.mysql.jdbc.Driver");
                            cnx=DriverManager.getConnection("jdbc:mysql://localhost:3306/paint","root","");
                            
                 }

        }
         
         abstract public boolean Ajout(T data) throws Exception;
         abstract public boolean Suppression(T data)throws Exception;
	 abstract public boolean Modification(T data)throws Exception;
	 abstract public  ArrayList<T> Lister()throws Exception;
         
    
}

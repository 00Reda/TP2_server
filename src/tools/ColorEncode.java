/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import java.awt.Color;

/**
 *
 * @author IdeaPad
 */
public class ColorEncode {
    
    public static String GetHex(Color c){
        
        return String.format("#%02x%02x%02x", c.getRed(), c.getGreen(), c.getBlue()); 
        
    }
    
   
}

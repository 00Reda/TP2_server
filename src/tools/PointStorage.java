/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tools;

import java.awt.Point;
import java.util.ArrayList;

/**
 *
 * @author IdeaPad
 */
public class PointStorage {
    
    public static String FromPointToString(Point p){
        return p.x+","+p.y;
    }
    
    public static Point FromStringToPoint(String str){
       String[] strs= str.split(",");
       return new Point(Integer.valueOf(strs[0]), Integer.valueOf(strs[1]));
    }
    
    public static String PolylineToString(ArrayList<Point> list){
        StringBuffer b=new StringBuffer();
        for (int i=0;i<list.size();i++) {
            
            b.append(list.get(i).x+","+list.get(i).y);
            if(i!=list.size()-1){
                b.append("/");
            }
        }
        
        return b.toString();
    }
    
    public static ArrayList<Point> StringToPolyline(String str){
        String[] strs= str.split("/");
        ArrayList<Point> listPt=new ArrayList<>();
        for (String one : strs) {
            listPt.add(PointStorage.FromStringToPoint(one));
                   
        }
        return listPt;
    }
    
}
